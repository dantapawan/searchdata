# Search Data



## Getting started

1. Import as a maven project
2. After import run mvn clean install
3. This project require java 8 and Maven build tool
4. <b> After the project is imported run the command "mvn clean install" </b>

### Note

The org.tch.service.SearchService class has all the logic for all type of searches.

Added the test cases in SearchServiceTest.


## Running the program

mvn exec:java -Dexec.mainClass="org.tch.Main"

<br/><br/>


[INFO] Scanning for projects...

[INFO]

[INFO] -----------------------< org.example:searchData >-----------------------

[INFO] Building searchData 1.0-SNAPSHOT

[INFO] --------------------------------[ jar ]---------------------------------

[INFO]

[INFO] --- exec-maven-plugin:3.0.0:java (default-cli) @ searchData ---

<br/>


Enter 1 if you want search by field - name, type, city, state, zipcode

Enter 2 if you want search by field - city, state

1


Enter field name - name, type, city, state, zipcode :: name

Enter value :: Amazing Bank

BankInfo(id=1, bankName=Amazing Bank, type=Branch, city=New York, state=NY, zipCode=10018)
BankInfo(id=5, bankName=Amazing Bank, type=ATM, city=New York, state=NY, zipCode=10018)
BankInfo(id=7, bankName=Amazing Bank, type=ATM, city=Winston-Salem, state=NC, zipCode=27106)
BankInfo(id=11, bankName=Amazing Bank, type=ATM, city=Dallas, state=TX, zipCode=75202)






[INFO] ------------------------------------------------------------------------

[INFO] BUILD SUCCESS

[INFO] ------------------------------------------------------------------------

[INFO] Total time:  36.469 s

[INFO] Finished at: 2022-05-03T17:55:57-05:00

[INFO] ------------------------------------------------------------------------


