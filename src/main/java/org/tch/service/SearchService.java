package org.tch.service;

import org.tch.model.BankInfo;
import org.tch.util.CsvFileReader;

import java.util.*;
import java.util.stream.Collectors;

public class SearchService {
    private static Optional<List<BankInfo>> banks;

    Comparator<BankInfo> nameComparator = Comparator.comparing(BankInfo :: getBankName);
    Comparator<BankInfo> typeComparator = Comparator.comparing(BankInfo :: getType);
    Comparator<BankInfo> cityComparator = Comparator.comparing(BankInfo :: getCity);
    Comparator<BankInfo> stateComparator = Comparator.comparing(BankInfo :: getState);
    Comparator<BankInfo> zipCodeComparator = Comparator.comparing(BankInfo :: getZipCode);

    static {
        CsvFileReader csv = new CsvFileReader();
        try {
            banks = csv.readCsvFile();
        } catch (Exception ex) {
            System.err.println("Error reading the input file :: " + ex.getMessage());
        }
    }

    public void searchByCityState(String city, String state)  {
        Comparator<BankInfo> cityStateComparator = cityComparator.thenComparing(stateComparator);
        List<BankInfo> result =  banks.get().stream().sorted(cityStateComparator)
                .filter(x -> (x.getCity().equalsIgnoreCase(city) || x.getState().equalsIgnoreCase(state)))
                .collect(Collectors.toList());
        print(result);
    }

    public void searchByField(String fieldName, String value) {
        List<BankInfo> result;
        switch (fieldName) {
            case "name" : result = findByName(value);
                    break;
            case "type" : result = findByType(value);
                    break;
            case "city" : result = findByCity(value);
                break;
            case "state" : result = findByState(value);
                break;
            case "zipcode" : result = findByZipCode(value);
                break;
            default:
                result = banks.get();
                break;
        }
        print(result);
    }
    public List<BankInfo> findByName(String value){
         List<BankInfo> result =  banks.get().stream().sorted(nameComparator).filter(x -> x.getBankName().equalsIgnoreCase(value))
                .collect(Collectors.toList());
         return result;
    }

    public List<BankInfo> findByType(String value){
        List<BankInfo> result =  banks.get().stream().sorted(typeComparator).filter(x -> x.getType().equalsIgnoreCase(value))
                .collect(Collectors.toList());
        return result;
    }

    public List<BankInfo> findByCity(String value){
        List<BankInfo> result =  banks.get().stream().sorted(cityComparator).filter(x -> x.getCity().equalsIgnoreCase(value))
                .collect(Collectors.toList());
        return result;
    }

    public List<BankInfo> findByState(String value){
        List<BankInfo> result =  banks.get().stream().sorted(stateComparator).filter(x -> x.getState().equalsIgnoreCase(value))
                .collect(Collectors.toList());
        return result;
    }

    public List<BankInfo> findByZipCode(String value){
        List<BankInfo> result =  banks.get().stream().sorted(zipCodeComparator).filter(x -> x.getZipCode().equalsIgnoreCase(value))
                .collect(Collectors.toList());
        return result;
    }

    public void print(List<BankInfo> bankInfoList){
        if (Objects.nonNull(bankInfoList) && bankInfoList.size() > 0){
            bankInfoList.forEach(System.out:: println);
        } else {
            System.err.println("No data found for search criteria");
        }

    }


}
