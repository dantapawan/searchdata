package org.tch.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BankInfo {
    private String id;
    private String bankName;
    private String type;
    private String city;
    private String state;
    private String zipCode;


}
