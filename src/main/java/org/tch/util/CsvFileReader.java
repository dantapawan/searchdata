package org.tch.util;

import org.tch.model.BankInfo;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CsvFileReader {

    private final String inputFile = "ProgrammingAssignment.csv";


    public Optional<List<BankInfo>> readCsvFile() throws IOException, URISyntaxException {
        Pattern pattern = Pattern.compile(",");
        Path path = Paths.get(getClass().getClassLoader()
                .getResource(inputFile).toURI());
        List<BankInfo> banks;
        try (Stream<String> lines = Files.lines(path)){
            banks = lines.skip(1).map( line -> {
                String[] str = pattern.split(line);
                return BankInfo.builder().id(str[0]).bankName(str[1])
                        .type(str[2]).city(str[3]).state(str[4]).zipCode(str[5]).build();
            }).collect(Collectors.toList());
        }
        return Optional.ofNullable(banks);
    }
}
