package org.tch;

import org.tch.service.SearchService;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));

        System.out.println("Enter 1 if you want search by field - name, type, city, state, zipcode");
        System.out.println("Enter 2 if you want search by field - city, state");
        String option = reader.readLine();
        SearchService service = new SearchService();
        if ( option.equals("1") ) {
            System.out.print("Enter field name - name, type, city, state, zipcode :: ");
            String field = reader.readLine();
            System.out.print("Enter value :: ");
            String value = reader.readLine();
            service.searchByField(field, value);
        } else if (option.equals("2") ){
            System.out.print("Enter c1ity  :: ");
            String city = reader.readLine();
            System.out.print("Enter state :: ");
            String state = reader.readLine();
            service.searchByCityState(city, state);
        } else {
            System.out.println("Enter proper search option");
        }
    }


}