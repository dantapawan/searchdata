package org.tch.service;


import org.junit.jupiter.api.Test;
import org.tch.model.BankInfo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

public class SearchServiceTest {

    SearchService service = new SearchService();

    @Test
    public void findByNameTest() {
        List<BankInfo> banks = service.findByName( "Amazing Bank");
        assertEquals(4, banks.size());
        assertEquals("Amazing Bank", banks.get(0).getBankName());
    }

    @Test
    public void findByInvalidNameTest() {
        List<BankInfo> banks = service.findByName( "Invalid Name");
        assertEquals(0, banks.size());
    }

    @Test
    public void findByTypeTest() {
        List<BankInfo> banks = service.findByType( "ATM");
        assertEquals(6, banks.size());
        assertEquals("ATM", banks.get(0).getType());
    }

    @Test
    public void findByInvalidTypeTest() {
        List<BankInfo> banks = service.findByType( "Invalid Type");
        assertEquals(0, banks.size());
    }

    @Test
    public void findByCityTest() {
        List<BankInfo> banks = service.findByCity( "New York");
        assertEquals(5, banks.size());
        assertEquals("New York", banks.get(0).getCity());
    }

    @Test
    public void findByInvalidCityTest() {
        List<BankInfo> banks = service.findByCity( "Invalid City");
        assertEquals(0, banks.size());
    }


    @Test
    public void findByStateTest() {
        List<BankInfo> banks = service.findByState( "NY");
        assertEquals(5, banks.size());
        assertEquals("NY", banks.get(0).getState());
    }

    @Test
    public void findByInvalidStateTest() {
        List<BankInfo> banks = service.findByState( "Invalid State");
        assertEquals(0, banks.size());
    }

    @Test
    public void findByZipCodeTest() {
        List<BankInfo> banks = service.findByZipCode( "10018");
        assertEquals(2, banks.size());
        assertEquals("10018", banks.get(0).getZipCode());
    }

    @Test
    public void findByInvalidZipCodeTest() {
        List<BankInfo> banks = service.findByZipCode( "Invalid ZipCode");
        assertEquals(0, banks.size());
    }
}
